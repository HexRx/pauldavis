    <?php get_header(); ?>

    <div class="row slider">
        <img class="image" src="<?php bloginfo('template_url'); ?>/images/slide.jpg">
        <div class="slider-block col-md-4 hidden-xs hidden-sm">
            <div class="row icon-block">
                <div class="left no-padding">
                    <div class="sprite slider-block-red-house"></div>
                </div>
                <div class="right">
                    <div>Learn about our</div>
                    <div class="big">Emergency Service</div>
                    <div class="italic">Franchise Opportunity</div>
                </div>
            </div>
            <div class="middle-block">Are you an <i>experienced</i> contractor?</div>
            <div class="row icon-block">
                <div class="left no-padding">
                    <div class="sprite slider-block-green-house"></div>
                </div>
                <div class="right">
                    <div>Learn about our</div>
                    <div class="big">Restoration</div>
                    <div class="italic">Franchise Opportunity</div>
                </div>
            </div>
        </div>
        <div class="slider-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-1">
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/john_smith_guy.png">
                    </div>
                    <blockquote class="col-md-7 col-sm-7 col-xs-6">
                        <p class="text">I wanted to help my community and work with an industry leader.</p>
                        <footer class="author">John Smith, Paul David Franchisee of Green Bay</footer>
                    </blockquote>
                    <div class="col-md-2 col-sm-2 col-xs-3 right no-padding-right">
                        <div class="big">Meet John Smith</div>
                        <div class="italic">Paul Davis of Green Bay</div>
                        <a class="arrow" href=""><span class="sprite slider-right-arrow"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row find-out-more-block-before">
        <div class="col-sm-6 border">
            <div class="icon-block">
                <div class="left no-padding">
                    <div class="sprite slider-block-red-house"></div>
                </div>
                <div class="right">
                    <div>Learn about our</div>
                    <div class="big">Emergency Service</div>
                    <div class="italic">Franchise Opportunity</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="middle-block">Experience Contractor?</div>
            <div class="icon-block">
                <div class="left no-padding">
                    <div class="sprite slider-block-green-house"></div>
                </div>
                <div class="right">
                    <div>Learn about our</div>
                    <div class="big">Restoration</div>
                    <div class="italic">Franchise Opportunity</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row find-out-more-block">
        <div class="text">Paul Davis is the leading emergency service and property damage reconstruction provider in North America, with more than <strong>300 franchise locations across the U.S.</strong></div>
        <a class="btn" href="">Find out more<i class="sprite btn-arrow-2"></i></a>
    </div>

    <div class="row">
        <div class="container">
            <div class="row info-block collaboration-block">
                <div class="col-md-5 text-center">
                    <div class="row">
                        <div class="sprite collaboration"></div>
                    </div>
                    <div class="title-block">Collaboration<div> & Team Work</div></div>
                    <div class="visible-xs visible-sm image">
                        <img src="<?php bloginfo('template_url'); ?>/images/slide_2.jpg">
                    </div>
                    <div>
                        <div class="hidden-sm">Collaboration's not just a buzzword at PDR:</div>

                        <p>Our franchisees help shape corporate police.</p>

                        <div>Learn how franchisee & corporate collaboration strengthens our company.</div>
                    </div>
                    <div>
                        <a class="btn btn-style-3" href="">Learn more</a>
                    </div>
                </div>
                <div class="col-md-7 hidden-xs hidden-sm">
                    <img src="<?php bloginfo('template_url'); ?>/images/slide_2.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="row info-block our-heritage-block">
        <div class="container">
            <div class="row">
                <div class="sprite our-heritage"></div>
                <div class="title-block">Our Heritage</div>
                <div class="description">
                    <div class="hidden-sm">Paul Davis Restoration is a respected, innovative branding, helping home and business owners to restore peace after the storm for more than 50 years.</div>
                    <div class="visible-sm">Lorem ipsum dolor sit amet</div>
                </div>
                <div class="blocks">
                    <div class="col-md-4 col-sm-4">
                        <div class="block">
                            <div class="row title">Years of Franchising Experience</div>
                            <div class="separator"></div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum enim non nunc viverra posuere bibendum enim non nunc viverra posuere.. Etiam et vestibulum dolor.</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="block">
                            <div class="row title">Two Models, One Team</div>
                            <div class="separator"></div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum enim non nunc viverra posuere bibendum enim non nunc viverra posuere.. Etiam et vestibulum dolor.</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="block">
                            <div class="row title">Industry Leader</div>
                            <div class="separator"></div>
                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum enim non nunc viverra posuere bibendum enim non nunc viverra posuere.. Etiam et vestibulum dolor.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="row info-block support-block">
                <div class="col-md-7 no-padding-left hidden-xs hidden-sm">
                    <img src="<?php bloginfo('template_url'); ?>/images/slide_3.jpg">
                </div>
                <div class="col-md-5 text-center">
                    <div class="sprite support"></div>
                    <div class="title-block">Support & Training</div>
                    <div class="col-md-7 no-padding-left visible-xs visible-sm image">
                        <img src="<?php bloginfo('template_url'); ?>/images/slide_3.jpg">
                    </div>
                    <div>Meet the experts who provide industry-leading training at our state-of-the-art facility.</div>
                    <div>
                        <a class="btn btn-style-3" href="">Meet john</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
