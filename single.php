<?php get_header(); ?>

<div class="single">
	<?php while ( have_posts() ) { the_post();

		get_template_part( 'content', get_post_format() );

	} ?>
</div>

<?php get_footer(); ?>
