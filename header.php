<!DOCTYPE html>
<html lang="en">
<head>
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300,300italic' rel='stylesheet' type='text/css'>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<div class="container-fluid">

	<div class="row">
		<div class="container">
			<div class="row header">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/logo.png">
					</a>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-6 pull-right contacts">
					<div class="phone">1-800-722-5066</div>
					<div>
						<a class="btn pull-left contact-us">Contact us<i class="sprite btn-arrow-1 hidden-sm hidden-xs"></i></a>
						<div class="socials">
							<a class="pull-left" href=""><div class="sprite youtube"></div></a>
							<a class="pull-left" href=""><div class="sprite twitter"></div></a>
							<a class="pull-left" href=""><div class="sprite linkedin"></div></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<nav class="row nav-header">
		<div class="container">
			<div class="row">
				<?php wp_nav_menu( array( 'menu_class' => 'nav hidden-sm hidden-xs', 'walker' => new Custom_Head_Nav_Menu() ) ); ?>

				<ul class="nav visible-sm visible-xs">
					<li class="col-sm-1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class="sprite home"></span></a></li>
					<li class="col-sm-1 pull-right"><a id="side-menu" href="#sidr"><span class="sprite hamburger"></span></a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div id="sidr">
		<?php wp_nav_menu( array( 'menu_class' => 'nav', 'walker' => new Custom_Simple_Nav_Menu() ) ); ?>
	</div>
	<script>
		jQuery(document).ready(function() {
			var sidr_status = false;
			jQuery('#side-menu').sidr({
				side: 'right'
			});
			jQuery('#side-menu').click(function (e) {
				e.preventDefault();
				if (sidr_status){
					jQuery.sidr('close', 'sidr');
					sidr_status = false;
				} else {
					jQuery.sidr('open', 'sidr');
					sidr_status = true;
				}
			});
		});
	</script>