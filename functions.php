<?php

function enqueue_styles() {
	wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
	wp_enqueue_style( 'main-style', get_stylesheet_uri() );
	wp_enqueue_style( 'jquery.sidr.dark', get_template_directory_uri() . '/css/jquery.sidr.dark.css' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

function enqueue_scripts() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery.sidr', get_template_directory_uri() . '/js/jquery.sidr.min.js' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'menus' );
}

/**
 * Header menu
 *
 * Class Custom_Head_Nav_Menu
 */
class Custom_Head_Nav_Menu extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth, $args ) {

		// Tag li css classes
		$classes = array();
		$classes = array_merge( $item->classes, $classes );

		// Add 'active' class
		$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$item_url = esc_attr( $item->url );
		if ( $item_url == $current_url ) {
			$classes[] = 'active';
		}

		$is_home = false;
		if ( in_array( 'menu-item-home', $item->classes ) ) {
			$is_home = true;

			$classes[] = 'col-md-1 no-padding';
		}

		$class_names = implode( ' ', $classes );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		$output .= '<li id="menu-item-' . $item->ID . '"' . $class_names . '>';

		$attributes = '';
		$attributes .= !empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

		$item_output = $args->before;

		if ( $is_home ) {
			// Show 'Home' icon
			$item_output .= '<a' . $attributes . '><span class="sprite home"></span></a>';
		} else {
			$item_output .= '<a' . $attributes . '>' . $item->title . '</a>';
		}

		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

/**
 * Menu without home icon
 *
 * Class Custom_Simple_Nav_Menu
 */
class Custom_Simple_Nav_Menu extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth, $args ) {
		// Tag li css classes
		$classes = array();
		$classes = array_merge( $item->classes, $classes );

		// Add 'active' class
		$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$item_url = esc_attr( $item->url );
		if ( $item_url == $current_url ) {
			$classes[] = 'active';
		}

		$class_names = implode( ' ', $classes );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		$output .= '<li id="menu-item-' . $item->ID . '"' . $class_names . '>';

		$attributes = '';
		$attributes .= !empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

		$item_output = $args->before;

		$item_output .= '<a' . $attributes . '>' . $item->title . '</a>';

		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

/**
 * Breadcrumbs
 */
function custom_breadcrumbs() {
	if ( !is_home() ) {
		echo '<a href="';
		echo get_option( 'home' );
		echo '">Home</a> / ';
		if ( is_category() || is_single() ) {
			the_category( 'title_li=' );
			if ( is_single() ) {
				echo ' / ';
				the_title();
			}
		} elseif ( is_page() ) {
			echo the_title();
		}
	}
}

/**
 * Fix image width
 *
 * Class fixImageMargins
 */
class fixImageMargins {
	public $xs = 0;

	public function __construct() {
		add_filter( 'img_caption_shortcode', array( &$this, 'fixme' ), 10, 3 );
	}

	public function fixme( $x = null, $attr, $content ) {

		extract( shortcode_atts( array(
			'id'       => '',
			'align'    => 'alignnone',
			'width'    => '',
			'caption'  => ''
		), $attr ) );

		if ( 1 > ( int ) $width || empty( $caption ) ) {
			return $content;
		}

		if ( $id ) {
			$id = 'id="' . $id . '" ';
		}

		return '<div ' . $id . 'class="wp-caption ' . $align . '" style="width: ' . ( ( int ) $width + $this->xs ) . 'px">'
		. $content . '<p class="wp-caption-text">' . $caption . '</p></div>';
	}
}
$fixImageMargins = new fixImageMargins();