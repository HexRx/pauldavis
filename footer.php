	<div class="row why-block">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 no-padding">
					<p class="title">Why Paul Davis?</p>
					<p class="big">Paul Davis Restoration is the leading emergency service and property damage reconstruction provider in North America, with more than 300 franchise locations. Our experience and dedication to providing best-in-class training and support to both our clients and franchisees makes us the leading restoration company in the business.</p>
					<p class="small">If you're dedicated, ready to learn, and want to take control of your financial future, explore owning a Paul Davis Restoration business. It just might be the opportunity you've been waiting for.</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-1">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-5 image">
							<img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo.png">
						</div>
						<div class="col-md-12 col-sm-12 col-xs-7 facebook-widget">
							<div class="fb-page"
								 data-width="302"
								 data-height="220"
								 data-href="https://www.facebook.com/facebook"
								 data-tabs="timeline"
								 data-small-header="false"
								 data-adapt-container-width="true"
								 data-hide-cover="false"
								 data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="container">
			<div class="row partners-block">
				<div class="title hidden-sm hidden-xs"><div class="col-md-12">A prevent model:</div></div>
				<div>
					<div class="col-md-8 col-md-offset-0 col-xs-offset-1 no-padding-right">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_1.png">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_2.png">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_3.png">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_4.png">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_5.png">
					</div>
					<div class="col-md-4 no-padding bottom">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_6.png">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_7.png">
						<img src="<?php bloginfo('template_url'); ?>/images/partner_8.png">
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="row footer">
		<div class="container">
			<div class="row">
				<?php wp_nav_menu( array( 'menu_class' => 'nav col-md-8 col-md-offset-0 col-sm-offset-1', 'walker' => new Custom_Simple_Nav_Menu() ) ); ?>

				<div class="col-md-4 nav-right">
					<ul class="list-inline">
						<li><a href="">Sitemap</a></li>
						<li><a href="">Private Policy</a></li>
						<li><a href="">Terms & Conditions</a></li>
					</ul>
					<div>©2016 All Rights Reserved</div>
				</div>
			</div>
		</div>
	</footer>

</div>

	<?php wp_footer(); ?>
</body>
</html>