<article>
	<div class="row header">
		<div class="container">
			<div class="pull-left title"><?php the_title(); ?></div>
			<div class="pull-right breadcrumbs"><?php custom_breadcrumbs(); ?></div>
		</div>
	</div>
	<div class="row">
		<div class="container">
			<div class="content"><?php the_content(); ?></div>
		</div>
	</div>
</article>